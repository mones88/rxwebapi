# Reactive Web API #

Return IObservables through Asp NET Web APIs!

### How do I get set up? ###

Create a route to handle your observable apis:
```
#!c#

config.Routes.MapHttpRoute(
          name: "RxApi",
          routeTemplate: "rxapi/{controller}/{id}",
          defaults: new { id = RouteParameter.Optional },
          constraints: null,
          handler: new ReactiveHttpHandler(config)
);
```

Create a controller as you would normally do then create actions like:

```
#!c#

public IObservable<ComplexValue> Get()
{
	return Observable.Range(0, 10)
		.Select(i => new ComplexValue()
		{
			Id = i,
			Username = "username " + i
		});
}
```

Client side you can use javascript like:

```
#!c#

var last_response_len = false;
$.ajax('http://localhost:62479/rxapi/Values', {
    xhrFields: {
        onprogress: function (e) {
            var this_response, response = e.currentTarget.response;
            if (last_response_len === false) {
                this_response = response;
                last_response_len = response.length;
            }
            else {
                this_response = response.substring(last_response_len);
                last_response_len = response.length;
            }
                    
            var obj = $.parseJSON(this_response);

            var receivedData= '';
            receivedData += obj.Id;
            receivedData += ' - ';
            receivedData += obj.Username;

            alert(receivedData);
        }
    }
})
.done(function (data) {
    alert("Table load complete!");
})
.fail(function (data) {
    alert("Error loading table: " + data);
});
```