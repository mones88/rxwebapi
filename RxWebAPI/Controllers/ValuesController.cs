﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reactive.Linq;
using System.Threading;
using System.Web.Http;

namespace RxWebAPI.Controllers
{
    public class ComplexValue
    {
        public int Id { get; set; }
        public string Username { get; set; }
    }

    public class ValuesController : ApiController
    {
        public IObservable<ComplexValue> Get()
        {
            return Observable.Range(0, 10)
                .SelectMany(i =>
                {
                    Thread.Sleep(1000);
                    return Observable.Return(i);
                })
                .Select(i => new ComplexValue()
                {
                    Id = i,
                    Username = "username " + i
                });
        }

        // GET api/values/5
        public IObservable<ComplexValue> Get(int id)
        {
            return Observable.Return(new ComplexValue()
            {
                Id = id,
                Username = "username " + 1
            });
        }
        
    }
}
