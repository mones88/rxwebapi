﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace RxWebAPI.RxInfrastructure
{
    public class RxObserver<T> : IObserver<T>
    {
        Stream _httpOutStream;

        public RxObserver(Stream httpOutStream)
        {
            _httpOutStream = httpOutStream;
        }

        public void OnCompleted()
        {
            _httpOutStream.Close();
        }

        public void OnError(Exception error)
        {
            _httpOutStream.Close();
        }

        public void OnNext(T value)
        {
            var json = JsonConvert.SerializeObject(value);
            var buffer = Encoding.UTF8.GetBytes(json);
            _httpOutStream.Write(buffer, 0, buffer.Length);
        }
    }
}