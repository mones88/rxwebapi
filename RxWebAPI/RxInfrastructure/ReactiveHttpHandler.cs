﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net.Http;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Http.Routing;
using System.Linq.Expressions;
using System.Reflection;

namespace RxWebAPI.RxInfrastructure
{
    public class ReactiveHttpHandler : HttpMessageHandler
    {
        private IHttpControllerSelector _controllerSelector;
        private readonly HttpConfiguration _configuration;

        public ReactiveHttpHandler(HttpConfiguration configuration)
        {
            _configuration = configuration;
        }

        public HttpConfiguration Configuration
        {
            get { return _configuration; }
        }

        private IHttpControllerSelector ControllerSelector
        {
            get
            {
                if (_controllerSelector == null)
                {
                    _controllerSelector = _configuration.Services.GetHttpControllerSelector();
                }
                return _controllerSelector;
            }
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var response = request.CreateResponse();
            response.Content = new PushStreamContent(
                (outStream, httpContent, transportContext) =>
                {
                    var ris = SendAsyncInternal(request, cancellationToken);
                    var content = ris.Result.Content as ObjectContent;

                     var genericType = content.Value.GetType().GenericTypeArguments.Last();
                     var observableGenericType = typeof(IObservable<>).MakeGenericType(genericType);
                    
                    var param = Expression.Parameter(typeof(object));
                    var observable = Expression.Lambda(Expression.Convert(param, observableGenericType), param)
                        .Compile().DynamicInvoke(content.Value);

                    var observerType = typeof(RxObserver<>);
                    var observerGenericType = observerType.MakeGenericType(genericType);
                    var observer = Activator.CreateInstance(observerGenericType, new object[] { outStream });

                    var subscribeMethodInfo = observable.GetType().GetMethod("Subscribe", BindingFlags.Instance | BindingFlags.Public);
                    subscribeMethodInfo.Invoke(observable, new object[] { observer });
                    
                });
            return Task.FromResult(response);            
        }

        private Task<HttpResponseMessage> SendAsyncInternal(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            IHttpRouteData routeData = request.GetRouteData();
            Contract.Assert(routeData != null);

            HttpControllerDescriptor httpControllerDescriptor = ControllerSelector.SelectController(request);
            IHttpController httpController = httpControllerDescriptor.CreateController(request);

            HttpControllerContext controllerContext = new HttpControllerContext(_configuration, routeData, request);
            controllerContext.Controller = httpController;
            controllerContext.ControllerDescriptor = httpControllerDescriptor;

            return httpController.ExecuteAsync(controllerContext, cancellationToken);
        }
    }
}